---
layout: handbook-page-toc
title: "Data Team Principles"
description: "GitLab Data Team Handbook"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

---

## <i class="far fa-compass fa-fw color-orange font-awesome" aria-hidden="true"></i>Data Team Principles

The Data Team at GitLab is working to establish a world-class data analytics and engineering function by utilizing the tools of DevOps in combination with the core values of GitLab.
We believe that data teams have much to learn from DevOps.
We will work to model good software development best practices and integrate them into our data management and analytics.

A typical data team has members who fall along a spectrum of skills and focus.
For now, the data function at GitLab has Data Engineers and Data Analysts; eventually, the team will include Data Scientists. Review the [team organization section](/handbook/business-ops/data-team/#-team-organization) section to see the make up of the team.

Data Engineers are essentially software engineers who have a particular focus on data movement and orchestration.
The transition to DevOps is typically easier for them because much of their work is done using the command line and scripting languages such as bash and python.
One challenge in particular are data pipelines.
Most pipelines are not well tested, data movement is not typically idempotent, and auditability of history is challenging.

Data Analysts are further from DevOps practices than Data Engineers.
Most analysts use SQL for their analytics and queries, with Python or R.
In the past, data queries and transformations may have been done by custom tooling or software written by other companies.
These tools and approaches share similar traits in that they're likely not version controlled, there are probably few tests around them, and they are difficult to maintain at scale.

Data Scientists are probably furthest from integrating DevOps practices into their work.
Much of their work is done in tools like Jupyter Notebooks or R Studio.
Those who do machine learning create models that are not typically version controlled.
Data management and accessibility is also a concern.

We will work closely with the data and analytics communities to find solutions to these challenges.
Some of the solutions may be cultural in nature, and we aim to be a model for other organizations of how a world-class Data and Analytics team can utilize the best of DevOps for all Data Operations.

Some of our beliefs are:

- Everything can and should be defined in code
- Everything can and should be version controlled
- Data Engineers, Data Analysts, and Data Scientists can and should integrate best practices from DevOps into their workflow
- It is possible to serve the business while having a high-quality, maintainable code base
- Analytics, and the code that supports it, can and should be open source
- There can be a single source of truth for every analytic question within a company
- Data team managers serve their team and not themselves
- [Glue work](https://www.locallyoptimistic.com/post/glue-work/) is important for the health of the team and is recognized individually for the value it provides. [We call this out specifically as women tend to over-index on glue work and it can negatively affects their careers.](https://noidea.dog/glue)
- We focus our limited resources where data will have the greatest impact
- Lead indicators are just as important, if not moreso, than lag indicators
- All business users should be able to learn how to interpret and calculate simple statistics

---
