---
layout: handbook-page-toc
title: "Release:Release Management Group"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Product Vision

For an understanding of what this team is going to be working on take a look at [the product vision](/direction/ops/). 

We utilize the jobs-to-be-done (JTBD) framework to better understand our buyers' and users' needs. More information on our JTBD and 
their corresponding grade [can be viewed here](/handbook/engineering/development/ci-cd/release/release-management/jtbd). 

## Mission

The Release:Release Management is focused on all the functionality with respect to Continuous Delivery and Release Automation.

This team maps to [Release](/handbook/product/product-categories/#release-stage) devops stage.

#### OKRs
The EM is the DRI for the OKRs of the Release Management Engineering team.

| Period | Issue | 
| :---             |  ---:           |  
| FY21-Q2 | [https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7495](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7495) |
| FY21-Q3 | [https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8805](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8805) |

## Team Members

The following people are permanent members of the Release Team:

<!--<%= direct_team(manager_role: 'Backend Engineering Manager, Release:Release Management') %>-->
<%= direct_team(manager_role: 'Frontend Engineering Manager, Release (CD)', role_regexp: /Release Management/) %>

## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%= stable_counterparts(role_regexp: /Release Management/, direct_manager_role: 'Release Management', other_manager_roles: ['Frontend Engineering Manager, Release (CD)']) %>

## Technologies

Like most GitLab teams, we spend a lot of time working in Rails and Vue.js on the main [GitLab CE app](https://gitlab.com/gitlab-org/gitlab-ce), but we also do a lot of work in Go which is used heavily in [GitLab Pages](https://gitlab.com/gitlab-org/gitlab-pages) and the [Gitlab Release CLI](https://gitlab.com/gitlab-org/release-cli). Familiarity with Docker and Kubernetes is also useful on our team.

## Common Links

 * [Issue Tracker](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Arelease&label_name[]=group%3A%3Arelease%20management)
 * [Slack Channel](https://gitlab.slack.com/archives/CSLU7BMPF)
 * [Roadmap](/direction/release/)
 * [UX Strategy](/handbook/engineering/ux/stage-group-ux-strategy/release/)
 * [Problem Validation: Opportunity Canvases](https://gitlab.com/gitlab-org/ux-research/-/issues/778)

## Planning and Process

We are always striving to improve and iterate on our planning process as a team. To maximize our velocity and meet our deliverables, we follow a the process outlined on our 
[planning page](./planning.html).

## How to work with us

### On Issues

Issues that contribute to the release stage of the devops toolchain have the `~"devops::release"` label. Issues that belong to the release management group have the `~"group::release management"` label.

### In Slack

The group's primary Slack channel is `#g_release-management`. We also support a number of feature channels for discussons or questions about a specific [feature area](/handbook/product/product-categories/#release-stage). We no longer support issue specific channels, as they are easy to lose track of and fragment the discussion. Supported feature channels are:

* Release orchestration: `f_release_orchestration`
* Release governance: `f_release_governance`
* Review apps: `f_review_apps`
* Secrets management: `f_secrets_management`
* Vault integration: `f_vault_integration_secrets_mgmt`
* Pages: `gitlab-pages`
* Deploy freezes: `f_deploy_freezes`

### Team Workflow

Below is a description of each stage of our workflow, its meaning, and any requirements for that stage.

* `workflow::problem validation`
  * This stage aims to produce a [clear and shared understanding of the customer problem](/handbook/product-development-flow/#validation-phase-2-problem-validation), as well as an estimation of the engineering effort.
* `workflow::solution validation`
  * The output of this stage is a [clear prototype of the solution to be created](/handbook/product-development-flow/#validation-phase-4-solution-validation).
* `workflow::scheduling`
  * An issue in this stage has been refined and estimated, but is waiting for scheduling and not yet active.
* `workflow::ready for development`
  * This stage indicates the issue is ready for engineering to begin their work. 
  * Issues in this stage must have a `UX Ready` label, as well as either `frontend`, `backend` or both labels to indicate which areas will need focus.
* `workflow::in dev`
  * This stage indicates that the issue is actively being worked on by one or more developers.
* `workflow::in review`
  * This stage indicates that the issue is undergoing code review by the development team and/or undergoing design review by the UX team.
* `workflow::verification`
  * This stage indicates that everything has been merged and the issue is waiting for verification after a deploy.

### Engineering Evaluation & Estimation

In order to make sure sprints are effective as possible, it is important to ensure issues are clearly defined and broken down before the start of a sprint. In order to accomplish this, engineering evaluation will take place for issues that require it (small changes and bug fixes typically won’t require this analysis). Engineers will look for assigned issues with the `workflow::planning breakdown` label that have user stories defined, as well as functional, performance, documentation, and security acceptance criteria. See [Build phase](/handbook/product-development-flow/#build-phase-1-plan) of the Product Development Workflow.

Assigned engineers will work with Product, UX and other engineers to determine the implementation plan for the feature. ***Tip:** When you're having hard time to estimate MR count, [consider a PoC step](./poc.html).*

Once an implementation plan has been finalized, the following steps should be taken:

* The issue description should be updated to include the details of the implementation plan along with a checklist to show the planned breakdown of merge requests.
* The weight of the issue should be updated to reflect the number of merge requests estimated as part of the implementation plan. For smaller changes and bugs, the weight should be 1.
* The issue should be moved forward in the workflow to `workflow::scheduling`.
* Those updates to the issue will signal that the issue has been properly scoped and is ready to be worked on in an upcoming milestone.

#### Merge request count as a measure of issue weight

As a byproduct of the engineering evaluation process, a rough estimate of the number of merge requests required to develop a feature will be produced. This measure can be used as a way to determine issue weights. These weights can be useful during the planning process to get a rough gauge of how many features can be worked on in a milestone, and to understand how much work the team can do in a milestone. This metric also aligns with the [throughput metric](/handbook/engineering/management/throughput/) currently measured by engineering teams.

### Code Review

Code reviews follow the standard process of using the [reviewer roulette](https://docs.gitlab.com/ee/development/code_review.html#reviewer-roulette) to choose a reviewer and a maintainer. The roulette is optional, so if a merge request contains changes that someone outside our group may not fully understand in depth, it is encouraged that a member of the Release team be chosen for the preliminary review to focus on correctly solving the problem. The intent is to leave this choice to the discretion of the engineer but raise the idea that fellow Release team members will sometimes be best able to understand the implications of the features we are implementing. The maintainer review will then be more focused on quality and code standards.

This tactic also creates an environment to ask for early review on a WIP merge request where the solution might be better refined through collaboration and also allows us to share knowledge across the team.

### Async Status Updates

Since we are a [remote](/company/culture/all-remote/) company, we depend on asynchronous updates to communicate the status of our work. We currently use 3 methods to communicate with the team, two occur daily and one occurs weekly:

#### Async Issue Updates

Our daily updates on progress and status will be added to the issues as a comment. A daily update may be skipped if there was no change in progress. It's preferable to update the issue rather than the related merge requests, 
as those do not provide a view of the overall progress.

The status comment should include what percentage complete the work is, the confidence of the person that their estimate is correct and, notes on what was done and/or if review has started. Finally, if there are multiple MRs associated with an issue, please include an entry for each. 

A couple of suggestions to consider when adding your async updates:
* It may be good to include whether this is a front end or back end update if there are multiple people working on it
* Adding technical notes as you work through an issue is a great way to create an opening for others to engage and collaborate.

Examples:

```
Complete: 80%
Confidence: 90%
Notes: expecting to go into review tomorrow
Concern: ~frontend
```

```
Issue status: 20% complete, 75% confident
MR statuses:
!11111 - 80% complete, 99% confident - docs update - need to add one more section
!21212 - 10% complete, 70% confident - api update - database migrations created, working on creating the rest of the functionality next
```

#### Daily Standup

The [Geekbot](https://geekbot.io/) Daily Standup is configured to run each morning Monday through Thursday and posts to `#g_release-management` Slack channel. It has just one question:

1. ***Is there anything you could use a hand with today, or any blockers?***

    This check-in is **optional** and can be skipped if you don't need any help or don't have any blockers. Be sure to ask for help early, your team is always happy to lend a hand.

#### Weekly Status Update

The **Weekly Status Update** is configured to run at noon on Fridays on the `#g_release-management` Slack channel, and contains three questions:

1. ***What were your accomplishments this week?*** 

    The goal with this question is to show off the work you did, and **celebrate merge announcements** (merge parrot!). This could be a whole feature, a small part of a larger feature, an API to be used later, or even just a copy change.

2. ***What do you plan to work on next week?*** (think about what you'll be able to merge by the end of the week)

    Think about what the next most important thing is to work on, and think about what part of that you can accomplish in one week. If your priorities aren't clear, talk to your manager.

3. ***Who will you need help from to accomplish your plan for next week?*** (tag specific individuals so they know ahead of time)

    This helps to ensure that the others on the team know you'll need their help and will surface any issues earlier.

#### Taking Time Off

When going out of office (OOO), be sure to clearly communicate it with other people. 

1. In [PTO Ninja](/handbook/paid-time-off/#pto-ninja), set a role for your backup person during your PTO. If you will be out for an extended period (greater than two days), 
consider assigning the team slack channel `#g_release-management` as your backup to help distribute the workload. 
1. Add the [Release:RM Shared Calendar](https://calendar.google.com/calendar/embed?src=gitlab.com_8josslksslj9avafu2p77csbkc%40group.calendar.google.com&ctz=America%2FChicago) to your PTO Ninja settings so your PTO events are visible to everyone in the team. The calendar ID is: `gitlab.com_8josslksslj9avafu2p77csbkc@group.calendar.google.com`

Read more in the [Paid time off](/handbook/paid-time-off/#a-gitlab-team-members-guide-to-time-off) page.
