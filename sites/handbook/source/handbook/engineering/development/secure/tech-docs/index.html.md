---
layout: handbook-page-toc
title: Secure Technical Documentation
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Architecture

## Researches

## Brown bag sessions

Secure team members also share knowledge through [brown bag sessions](https://gitlab.com/gitlab-org/secure/brown-bag-sessions#brown-bag-sessions) on various topics.
