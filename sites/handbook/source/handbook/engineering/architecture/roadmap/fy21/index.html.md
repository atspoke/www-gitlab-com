---
layout: markdown_page
title: "FY21 Architecture Roadmap"
---

## FY21 Architecture Roadmap

1. [GitLab.com Migration to Cloud Native](https://about.gitlab.com/handbook/engineering/infrastructure/production/kubernetes/gitlab-com/)
   * [Cloud Native Build Logs](../blueprints/cloud_native_gitlab/cloud_native_build_logs)
   * [Cloud Native GitLab Pages](../blueprints/cloud_native_gitlab/cloud_native_gitlab_pages)
