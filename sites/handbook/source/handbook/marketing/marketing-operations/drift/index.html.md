---
layout: handbook-page-toc
title: "Drift"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Using Drift

Drift is a chat platform our Sales Development team uses to engage site visitors. 

### Drift requests & questions 
* **Slack channel:** #drift
* **Support:** Chat support (bottom left-hand corner of the Drift app)
* **MktgOps project issue:** State the reason for the issue and leave any detailed information in regards to the request being made. Use the following labels:
    * `MktgOps`: The starting point for any label that involves MktgOps. The MktgOps team subscribes to this label and will see the request being made.
    * `Drift`: Issue is directly related to the Drift chat tool. Specific admins for this tool are subscribed to this label.
    * `SDR`: If your request is from or involves the SDR team

### Rules of engagement  
-   Set yourself to away when done working / no longer want to receive chats. Please note: If you set yourself to away, prospects will still be able to interact with the bot, but once they answer the initial questions, they are automatically given the option to book time on your calendar. If you don’t set yourself to away they will have to wait while the bot attempts to notify you. 
-   SLA: please respond to chats within 30 seconds
-   If you are routed a chat from outside your territory, assist and qualify by continuing the conversation. If a lead is then created in Salesforce, change it to the appropriate owner.
-   In the event you aren't able to assist a site visitor on your own and escalation is needed, please reach out to your manager and/or the #drift slack channel

### Best practices
* Ensure you set yourself to away when you won't be able to receive Drift notifications. The prospect will still be able to chat with the bot but will be given the option to book a time with you right away as opposed to waiting for the bot to ping you. 
* As people have the ability to book meetings with you if you are offline or don't respond fast enough, every morning look at the meetings that were scheduled. You will receive an email invite. If you want to reach out prior to the meeting or potentially reschedule, please email the prospect in advance. 
* Ensure that you have browser notifications enabled to ensure you can respond quickly. If you don't respond within one minute, the prospect will be prompted to schedule time with you. *Note: if you are using the [Drift mobile app](https://gethelp.drift.com/hc/en-us/articles/360019664613-How-to-Use-the-Drift-Mobile-App) - turn notifications on within the app as well. 
* When available in Drift, we recommend having DataFox, Salesforce, the SSoT territory alignment, and Google Calendar open in other tabs to quckily look into things. 

### Working an inbound chat lead
1. Start the conversation with a professional greeting asking how you can help.
2. Begin to research the prospect with the information they have provided.
3. Use DataFox to begin confirming company details
4. Use Salesforce to see if this person is already in our system and in contact with another GitLabber (check for duplicates)
     - If you confirm that this prospect is note in your territory or wouldn't align to you, you can pull up the master territory spreadsheet to see which SDR should they should be working with. You can then add that SDR to [join the conversation](https://gethelp.drift.com/hc/en-us/articles/360019448174-How-to-Add-Participants-to-Conversations). If the appropriate SDR is unavailable, qualify to the best of your ability and ask if you can have the appropriate SDR or AE/SAL (for existing customers) reach out directly via email or phone. Relay this information with the appropriate GitLabber and share the lead/contact with them. (The Drift conversation will show up in the activity history section).
5. Qualify the prospect while assisting with their needs. 
6. If a meeting gets set up,  use the CQL “lightning” label on the right side of the drift browser to mark promising conversations. 

## Drift routing

Drift is expected to mirror [LeanData](/handbook/marketing/marketing-operations/leandata/) routing as closely as possible. If there is an exception it will be listed here as more complex routing is rolled out. Our goal is to only connect site visitors with their aligned resources in the SDR organization. This means site visitors during offline hours will be asked to schedule a meeting with their SDR. To update Drift team or routing please submit an issue using [this template](/marketing/marketing-operations/-/issues/new?issuable_template=leandata_change_sdralignment). 

## Drift resources & training materials

**Connect calendar and set hours**

*  Click your Avatar > Choose My Settings > Choose My Calendar and get connected.
*  Once connected, you can edit the following:
    * Description of the meeting
    * Duration of the meeting
    * Times you are available/time zone
    * Buffer time/minimum notice time

**Set yourself to available or away**

*  Change your status by clicking your avatar in the bottom left-hand corner. Whether you are available or away, you can still have your notifications on or off.
*  **Note:** If you close out of Drift, your status will not change. The only way to change your status is by updating in the tool.

**Chat notifications**
* There are various ways in which you can be notified of a chat. [This article](https://gethelp.drift.com/hc/en-us/articles/360019501974-How-to-Set-Up-Up-Your-Notifications-Preferences) will walk you through how to set up or change notifications. 

**Close or open a conversations**
*  You can change your conversation status by clicking the drop-down in the top right-hand corner of your conversation.

**Marking a chat as a CQL**
*  A CQL is a Chat Qualified Lead. 
*  On the right hand side of your conversations view, you will be able to see information about the site visitor. Right below their name, you will see the four options of CQL in the form of a lightning bolt. Please mark a lead with the red line for `unqualified lead`, one lightning bolt for `Inquiry`, and three lightning bolts for `MQL`. Refer to this [term glossary](/handbook/business-ops/resources/#glossary) for a refresher on `Inquiry` and `MQL` definitions. 

**Sharing your calendar in chat**
*  At the right-hand side of the conversation toolbar there is a small calendar icon. Click there and you can share your own calendar or anyone else’s calendar that is connected in Drift.

## Drift playbook
We have one Drift playbook that will lead a prospect down different routes depending on whether an SDR in their territory is available or away. If the SDR is available, the prospect will answer a series of preliminary questions and then route to the appropriate SDR. That SDR will get a notification on their browser and or mobile app to hop in the conversation. If the aligned SDR is away, the prospect will follow the same path but will be prompted to book time on the SDR's calendar right after answering the questions.

**Live pages**:
* https://about.gitlab.com/pricing/
* https://about.gitlab.com/sales/ 
* https://about.gitlab.com/free-trial/ 
* https://about.gitlab.com/features/ 
* https://about.gitlab.com/comparison/
